package com.featzima.ui;

import android.content.Context;
import android.os.Bundle;
import android.view.View;

import com.featzima.domain.AnalyticsEvent;
import com.featzima.domain.ApplicationContext;
import com.featzima.domain.IActivityProcessor;
import com.featzima.domain.IAnalyticsRepository;
import com.featzima.domain.ILogger;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.Set;

import javax.inject.Inject;
import javax.inject.Provider;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import dagger.android.AndroidInjection;
import dagger.android.support.DaggerAppCompatActivity;

public class MainActivity extends DaggerAppCompatActivity implements MainView {

    @Inject
    Set<ILogger> loggers;

    @Inject
    Set<IActivityProcessor> activityProcessors;

    @Inject
    @ApplicationContext
    Context context;

    @Inject
    MainPresenter presenter;

    @Inject
    Provider<IAnalyticsRepository> analyticsRepositoryProvider;

    // TODO FOR MOXY (
//    @ProvidePresenter
//    MainPresenter provideDetailsPresenter() {
//        return activityInjector.mainPresenter();
//    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        for (IActivityProcessor activityProcessor : activityProcessors) {
            activityProcessor.onCreate(this);
        }
        presenter.setView(this);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                DateStyleProvider dateStyleProvider = DateStyleProviderFactory.create();
//                DateFormat dateFormat = DateFormatFactory.create(dateStyleProvider);
//                Logger logger = LoggerFactory.create(dateFormat, dateStyleProvider);
                for (ILogger logger : loggers) {
                    logger.print("MainActivity", "Fab is clicked");
                }
                presenter.loadUserName();

                analyticsRepositoryProvider
                        .get()
                        .sendEvent(new AnalyticsEvent("fab", "fab", "fab"));
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
//        Logger logger = LoggerFactory.create(DateFormatFactory.create());
        for (ILogger logger : loggers) {
            logger.print("MainActivity", "Fab is clicked");
        }
    }

    @Override
    public void setUserName(String userName) {
        getSupportActionBar().setTitle(userName);
    }
}
