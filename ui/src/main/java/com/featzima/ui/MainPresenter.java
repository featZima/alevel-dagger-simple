package com.featzima.ui;

import android.annotation.SuppressLint;
import android.content.Context;


import com.featzima.domain.ActivityContext;
import com.featzima.domain.IUserRepository;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class MainPresenter {

    private final IUserRepository userRepository;
    private Context context;
    private MainView mainView;

    @Inject
    public MainPresenter(@ActivityContext Context context, IUserRepository userRepository) {
        this.userRepository = userRepository;
        this.context = context;
    }

    @SuppressLint("CheckResult")
    public void loadUserName() {
        userRepository
                .getUserName()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<String>() {
                    @Override
                    public void accept(String s) throws Exception {
                        mainView.setUserName(s);
                    }
                });
    }

    public void setView(MainView mainView) {
        this.mainView = mainView;
    }
}
