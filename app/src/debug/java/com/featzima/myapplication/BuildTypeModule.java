package com.featzima.myapplication;

import com.featzima.domain.ILogger;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoSet;

@Module
public interface BuildTypeModule {


    @Binds
    @IntoSet
    ILogger provideLogger(Logger logger);
}
