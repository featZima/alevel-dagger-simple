package com.featzima.myapplication;


import com.featzima.domain.IApplicationProcessor;

import java.util.Set;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DaggerApplication;

public class MyApplication extends DaggerApplication {

    @Inject
    Set<IApplicationProcessor> applicationProcessors;

    @Override
    public void onCreate() {
        super.onCreate();
        for (IApplicationProcessor processor : applicationProcessors) {
            processor.onCreate(this);
        }
    }

    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        return DaggerApplicationComponent
                .builder()
                .setApplicationContext(this)
                .build();
    }

}
