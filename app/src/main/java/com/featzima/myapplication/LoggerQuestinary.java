package com.featzima.myapplication;

import android.util.Log;

import com.featzima.domain.ILogger;

import java.text.DateFormat;
import java.util.Date;

import javax.inject.Inject;

public class LoggerQuestinary implements ILogger {

    private final DateFormat dateFormat;

    @Inject
    public LoggerQuestinary(DateFormat dateFormat) {
        Log.e("???", "LoggerQuestinary::constructor()");
        this.dateFormat = dateFormat;
    }

    @Override
    public void print(String tag, String message) {
        Log.e(tag, dateFormat.format(new Date()) + message);
    }
}
