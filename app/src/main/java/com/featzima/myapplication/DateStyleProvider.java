package com.featzima.myapplication;

import java.text.SimpleDateFormat;

import javax.inject.Inject;

public class DateStyleProvider {

    @Inject
    public DateStyleProvider() {
    }

    int getDateStyle() {
        return SimpleDateFormat.SHORT;
    }

}
