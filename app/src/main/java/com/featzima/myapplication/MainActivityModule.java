package com.featzima.myapplication;

import android.content.Context;

import com.featzima.domain.ActivityContext;
import com.featzima.ui.MainActivity;

import androidx.fragment.app.FragmentManager;
import dagger.Module;
import dagger.Provides;

@Module
public class MainActivityModule {

    @Provides
    @ActivityContext
    Context provideActivityContext(MainActivity activity) {
        return activity;
    }

    @Provides
    FragmentManager provideFragmentManager(MainActivity activity) {
        return activity.getSupportFragmentManager();
    }
}
