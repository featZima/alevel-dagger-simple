package com.featzima.myapplication;

import android.app.Application;

import com.featzima.domain.IApplicationProcessor;

import dagger.Module;
import dagger.Provides;
import dagger.multibindings.IntoSet;

@Module
public class FacebookModule {

    @Provides
    @IntoSet
    IApplicationProcessor provideFacebookApplicationProcessor() {
        return new IApplicationProcessor() {
            @Override
            public void onCreate(Application application) {
                // FacebookSDK.initialize(application.getApplicationContext());
            }
        };
    }
}
