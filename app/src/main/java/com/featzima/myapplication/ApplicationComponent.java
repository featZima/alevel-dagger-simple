package com.featzima.myapplication;

import android.content.Context;

import com.featzima.domain.ApplicationContext;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjector;

@Component(modules = {
        MyModule.class,
        MyModule2.class,
        UiModule.class,
        BuildTypeModule.class,
        FacebookModule.class
})
@ApplicationScope
public interface ApplicationComponent extends AndroidInjector<MyApplication> {

    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder setApplicationContext(@ApplicationContext Context context);

        ApplicationComponent build();
    }

}
