package com.featzima.myapplication;

import java.text.DateFormat;

import dagger.Module;
import dagger.Provides;

@Module
public class MyModule {

    @Provides
    @ApplicationScope
    public DateFormat provideDateFormat(DateStyleProvider dateStyleProvider) {
        return DateFormat.getDateInstance(dateStyleProvider.getDateStyle());
    }
}
