package com.featzima.myapplication;

import android.util.Log;

import com.featzima.domain.ILogger;

import java.text.DateFormat;
import java.util.Date;

import javax.inject.Inject;

@ApplicationScope
public class Logger implements ILogger {

    private final DateFormat dateFormat;

    @Inject
    public Logger(DateFormat dateFormat) {
        Log.e("!!!", "Logger::constructor()");
        this.dateFormat = dateFormat;
    }

    @Override
    public void print(String tag, String message) {
        Log.e(tag, dateFormat.format(new Date()) + message);
    }
}
