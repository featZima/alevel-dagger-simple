package com.featzima.myapplication;

import com.featzima.data.AnalyticsRepositoryImpl;
import com.featzima.data.ScreenshotProtector;
import com.featzima.domain.IActivityProcessor;
import com.featzima.domain.IAnalyticsRepository;
import com.featzima.domain.ILogger;
import com.featzima.domain.IUserRepository;
import com.featzima.data.UserRepositoryImpl;

import java.util.Set;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoSet;
import dagger.multibindings.Multibinds;

@Module
public interface MyModule2 {

    @Multibinds
    Set<ILogger> provideEmptySet();

    @Binds
    IAnalyticsRepository provideAnalyticsRepository(AnalyticsRepositoryImpl repository);

    @Binds
    @IntoSet
    ILogger provideLoggerQuestinary(LoggerQuestinary logger);

    @Binds
    @ApplicationScope
    IUserRepository provideUserRepository(UserRepositoryImpl repository);

//    @Binds
//    @IntoSet
//    IActivityProcessor bindScreenshotProtector(ScreenshotProtector screenshotProtector);

    @Multibinds
    Set<IActivityProcessor> emptySet();
}
