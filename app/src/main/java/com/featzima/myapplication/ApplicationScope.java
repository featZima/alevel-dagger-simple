package com.featzima.myapplication;

import javax.inject.Scope;

@Scope
public @interface ApplicationScope {
}
