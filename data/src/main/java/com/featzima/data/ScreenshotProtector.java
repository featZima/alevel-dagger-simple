package com.featzima.data;

import android.app.Activity;
import android.view.WindowManager;

import com.featzima.domain.IActivityProcessor;

import javax.inject.Inject;

public class ScreenshotProtector implements IActivityProcessor {

    @Inject
    public ScreenshotProtector() {
    }

    @Override
    public void onCreate(Activity activity) {
        activity.getWindow().setFlags(
                WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE);
    }
}
