package com.featzima.data;

import com.featzima.domain.IUserRepository;

import javax.inject.Inject;

import io.reactivex.Single;

public class UserRepositoryImpl implements IUserRepository {

    @Inject
    public UserRepositoryImpl() {
    }

    @Override
    public Single<String> getUserName() {
        return Single.just("Dmytro");
    }
}
