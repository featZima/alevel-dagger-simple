package com.featzima.data;

import android.util.Log;

import com.featzima.domain.AnalyticsEvent;
import com.featzima.domain.IAnalyticsRepository;

import javax.inject.Inject;

public class AnalyticsRepositoryImpl implements IAnalyticsRepository {

    @Inject
    public AnalyticsRepositoryImpl() {
        Log.e("Analytics", "::constructor");
    }

    @Override
    public void sendEvent(AnalyticsEvent event) {
        Log.i("Analytics", event.getItemId() +", " + event.getItemName() + ", " + event.getCategory());
    }
}
