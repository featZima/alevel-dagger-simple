package com.featzima.domain;

public interface IAnalyticsRepository {

    void sendEvent(AnalyticsEvent event);
}
