package com.featzima.domain;

import io.reactivex.Single;

public interface IUserRepository {

    Single<String> getUserName();
}
