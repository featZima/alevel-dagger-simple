package com.featzima.domain;

import javax.inject.Qualifier;

@Qualifier
public @interface ActivityContext {
}
