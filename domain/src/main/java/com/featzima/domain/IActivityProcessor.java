package com.featzima.domain;

import android.app.Activity;

public interface IActivityProcessor {

    void onCreate(Activity activity);
}
