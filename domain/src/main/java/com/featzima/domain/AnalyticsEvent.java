package com.featzima.domain;

public class AnalyticsEvent {

    private final String itemId;
    private final String itemName;
    private final String category;

    public AnalyticsEvent(String itemId, String itemName, String category) {
        this.itemId = itemId;
        this.itemName = itemName;
        this.category = category;
    }

    public String getItemId() {
        return itemId;
    }

    public String getItemName() {
        return itemName;
    }

    public String getCategory() {
        return category;
    }
}
