package com.featzima.domain;

public interface ILogger {

    void print(String tag, String message);
}
