package com.featzima.domain;

import android.app.Application;

public interface IApplicationProcessor {

    void onCreate(Application application);
}
